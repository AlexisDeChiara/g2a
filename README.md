# Jeu d'échec G2A

Le projet **"Jeu d'éhec G2A"** est un projet java + maven réalisé par 8 élèves du groupe 2 de S2T 2019-2020

Membres du projet :
* Thomas Guiot
* Alexis De Chiara
* Léo Lebihan
* Vincent Moigno
* Théo Langlade
* Thimothé Queysselier
* Floran Tatopoulos
* Quentin Var


## Compilation du projet
Pour compiler, il suffit, dans un terminal et une fois dans le répertoire G2A, de réaliser la commande suivante :
```bash
./maven/bin/mvn compile
```
## Execution du projet

Pour executer, il suffit, dans un terminal, une fois dans le répertoire G2A et après avoir compilé, de réaliser la commande suivante :
```bash
./maven/bin/mvn exec:java
```

## Test du projet
Vous pouvez si vous le souhaiter tester le projet avec la commande ci-dessous :

```bash
.\maven\bin\mvn.cmd test
```

## Comment jouer ?

Il s'agit d'un jeu d'échec classic, une fois le projet executé, vous aurez la possibilité de lancer le mode console ou le mode interface en écrivant `console` ou `interface` dans le terminal.

**Mode console :**

On vous demandera d'abord soit de charger une sauvegarde (en .csv) en tapant "charger", soit de démarrer la partie normalement en tapant "démarrer". Si l'on choisi de charger un fichier, il faut écrire le chemin d'accès du fichier, par exemple `ressources\coup_du_berger.csv`.

Une fois le lancement de la partie réalisé, la plateau s'affiche, le joueur blanc qui est en bas commence : Il doit taper la case du pions qu'il veut bouger puis après un espace la case de là où il veut que le pion aille.
Par exemple `E2 E4` déplace le pion blanc (représenté par "Pb") vers la case f4, il est à noter que le code n'est pas sensible à la case ! A tout moment un joueur peut arrêter la partie en saissant `stop` ce qui quitera le mode console mais aussi le projet en lui même.
Si un joueur est en situation d'échec, il est averti et doit donc bougé son roi. Lorsqu'il y a échec et mat, les joueurs peuvent recommencer la partie en tapant `recommencer` et peut toujours arrêter le programme en saisissant 'stop'.

**Mode Interface :**

La partie commence directement, le joueur blanc situer en bas commence. Dans la barre des menu il y a :
* Le bouton partie qui comprend le bouton "Recommencer" pour recommencer directement la partie et le bouton "Charger" pour charger une sauvegarde (en .csv) depuis le gestionnaire des fichier.
* Le bouton "Personnalisation" qui permet de choisir entre 7 couleurs de plateau différentes

Lorsqu'il y a une erreur, un échec ou échec et mat, une fenêtre apparait et nous en averti. Dans le cas d'un échec et mat on nous demande si l'on veut recommencer ("oui") ou arrêter ("arrêter"), dans le cas où l'on clique sur "non", les joueurs ne peuvent plus jouer mais on toujours accès au menu.

## Conception

![Conception](Conception.png)












