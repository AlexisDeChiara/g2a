package echec;

public class Position {

    private int x;    //ATTENTION !!! x et y sont compris entre 0 et 7
    private int y;

    public Position(int x, int y) {
        this.x = x;
        this.y = y;
    }

    public Position(String s) throws Exception//Permet de crer une position à partir d'une instruction de type b1 ou c3
    {
        s = s.toUpperCase();
        if (s.length() != 2) {
            throw new Exception("Coordonnée invalide (nombre d'argument incorrect) !");
        } else if (s.charAt(0) > 'H' || s.charAt(0) < 'A' || s.charAt(1) > '8' || s.charAt(1) < '1') {
            throw new Exception("Coordonnée invalide !");
        } else {
            this.x = s.charAt(0) - 'A';
            this.y = s.charAt(1) - '1';
        }
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    @Override
    public boolean equals(Object o) {
        Position p = (Position) o;
		return this.x == p.x && this.y == p.y;
    }

    @Override
    public String toString() {
        return "[" + (this.x) + ", " + (this.y) + "]";
    }

    public boolean estValide() {
		return this.x <= 7 && this.x >= 0 && this.y <= 7 && this.y >= 0;
    }
}
