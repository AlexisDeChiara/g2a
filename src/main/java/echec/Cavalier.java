package echec;

import java.awt.*;

public class Cavalier extends Piece {

    public Cavalier(Position p, Plateau plat, String s, Color couleur) {
        super(p, plat, s, couleur);
    }

    public boolean peutSeDeplacer(Position p) {
    	if(this.getPosition().equals(p))
    	{
    		return false;
    	}
    	else if(this.getPlateau().piecePlateau(p) != null && this.getPlateau().piecePlateau(p).getCouleur().equals(this.getCouleur()))
    	{
    		return false;
    	}
    	else
    	{
            return (Math.abs(p.getY() - this.getPosition().getY()) == 2 && Math.abs(p.getX() - this.getPosition().getX()) == 1) || (Math.abs(p.getX() - this.getPosition().getX()) == 2 && Math.abs(p.getY() - this.getPosition().getY()) == 1);
    	}
    }
}