package echec;

import java.awt.*;
import java.util.Scanner;

public class Partie {

    public static Scanner scanner = new Scanner(System.in);

    private Joueur tourDuJoueur;//Joueur
    private Joueur joueurBlanc;    //Joueur contenant les piéces blanches
    private Joueur joueurNoir;    //Joueur contenant les piéces noires
    private Plateau plateau;
    private Interface fenetre;
    private boolean partie = true;

    public Partie()    //Permet d'initialiser une partie en mode console sans affichage
    {
        Plateau.chargerPositionPossible();
        this.fenetre = null;
        this.plateau = new Plateau(this);
        this.joueurBlanc = this.plateau.getJoueurBlanc();    //On l'instantie dans l'instatiation du plateau
        this.joueurNoir = this.plateau.getJoueurNoir();        //On l'instantie dans l'instatiation du plateau
        this.tourDuJoueur = this.joueurBlanc;
        this.partie = true;
    }

    public Partie(Interface fenetre)    //Permet d'initialiser une partie avec l'affichage
    {
        this.plateau = new Plateau(this);
        this.joueurBlanc = this.plateau.getJoueurBlanc();    //On l'instantie dans l'instatiation du plateau
        this.joueurNoir = this.plateau.getJoueurNoir();        //On l'instantie dans l'instatiation du plateau
        this.tourDuJoueur = this.joueurBlanc;
        this.fenetre = fenetre;
        this.fenetre.setPartie(this);
    }

    public static void main(String[] args) {
        System.out.println("Saisir 'console' ou '1' pour démarrer une partie en console de commande et 'interface' ou '2' pour la lancer avec une interface :");
        String str = Partie.scanner.nextLine();
        while (!str.equals("1") && !str.equals("2") && !str.toLowerCase().equals("console") && !str.toLowerCase().equals("interface"))    //Tant que la valeur saisi est incorrect
        {
            System.out.println("Saisi incorrect ! Taper 'console' ou '1' pour démarrer une partie en console de commande et 'interface' ou '2' pour la lancer avec une interface :");
            str = Partie.scanner.nextLine();
        }
        if (str.equals("1") || str.toLowerCase().equals("console")) {
            Partie.modeConsole();
        } else if (str.equals("2") || str.toLowerCase().equals("interface")) {
            Partie.modeInterface();
        }
    }

    public static void modeConsole() {
        Partie game = new Partie();
        game.debutPartie();
        String str;        //Chaine de caractère qui permet de gerer le scanner
        Position[] tab;    //Tableau de position permettant de déplacer les pieces
        while (game.partie) {
            try {
                System.out.println("Saisir l'instruction ou saisir 'stop' pour arreter la partie :");
                str = Partie.scanner.nextLine();
                if (str.equals("stop")) {
                    game.partie = false;    //On stop la boucle while
                    System.exit(0);            //On casse la fonction pour ne pas terminer les instructions suivantes
                }

                tab = game.commandeDeplacement(str);    //On applique l'instruction

                //Si le deplacement a reussi sans erreur on execute la suite :
                game.plateau.deplacerPiece(tab[0], tab[1]);    //On deplace la piece dans le tableau de String
                game.plateau.affichageConsole();            //Aprés l'avoir déplacer on affiche le plateau
                game.tourSuivant();                            //On passe au joueur suivant
                game.checkEtatPartie(false);                //On vérifie les différents etats possible (echec/pat/echec et mat)
            } catch (Exception e) {
                System.out.println(e.getMessage() + " Veuillez recommencez :");        //En cas d'erreur dans l'instruction
            }

        }
    }
    
    public static void modeInterface() {
        Partie game = new Partie(new Interface());
        Plateau.chargerPositionPossible();
        Piece.chargerImagePiece();
        try {
			game.plateau.chargerFichier("ressources/dispositionDepart.csv");
		} catch (Exception e) {
			e.printStackTrace();
		}
        game.fenetre.setVisible(true);
        game.fenetre.chargerPlateau();
    }

    public Position[] commandeDeplacement(String commande) throws Exception    //Renvoi un tableau de Postion contenant la position de départ et celle d'arrivé
    {
        if (commande.length() == 5)    //Si l'instruction n'a pas le bon nombre de charactère
        {
            String[] s = commande.split(" ");
            if (s.length != 2) {
                throw new Exception("Instruction incorrect (nombre de coordonnée incorrect) !");
            } else {
                Position[] tab = new Position[]{new Position(s[0]), new Position(s[1])};
                this.tourDuJoueur.demanderDeplacement(tab[0], tab[1]);
                return tab;
            }
        } else {
            throw new Exception("Il faut 5 caractères pour une instruction");
        }
    }

    public void checkEtatPartie(boolean debut)    //Permet de regarder après un tour si le joueur est en Echec et Mat ou si il y a Pat
    {
        if (this.tourDuJoueur.estEchecEtMat())        //On regarde si l'autre joueur est en echec et mat
        {
            System.out.println("Le joueur " + this.tourDuJoueur + " est en echec et mat. Le joueur " + this.autreJoueur(this.tourDuJoueur) + " a gagné.");
            this.finDePartie();
        } else if (this.tourDuJoueur.estPat() && !this.tourDuJoueur.estEnEchec()) {
            System.out.println("Partie nul, le joueur " + this.tourDuJoueur + " est Pat.");
            System.out.println("Si vous voulez abandonner taper oui : (sinon taper n'importe quoi)");
            String s = Partie.scanner.nextLine();
            if(s.equals("oui"))
            {
            	this.finDePartie();
            }
        } else if (!debut) {
            System.out.println("\nTour du joueur : " + this.tourDuJoueur + " | Situation d'echecs : " + this.tourDuJoueur.estEnEchec());
        }
    }

    public void finDePartie() {
        System.out.println("Si vous voulez recommencer une partie tapez 'recommencer' ou alors taper 'stop' pour arreter.");
        String str = Partie.scanner.nextLine();
        while (!str.equals("recommencer") && !str.equals("stop")) {
            System.out.println("Saisi incorrect ! Taper 'recommencer' pour recommencer une partie ou 'stop' pour arreter de jouer.");
        }
        if (str.equals("recommencer"))    //On recommence une partie
        {
            this.reintPartie();
            this.debutPartie();
        } else {
            System.out.println("Fin de l'execution du jeu.");
            this.partie = false;
        }
    }

    public void debutPartie() {
        this.plateau.initPlateau();
        this.plateau.chargerPlateau();
        this.plateau.affichageConsole();    //On affiche le plateau sur la console
        this.checkEtatPartie(true);	//On check l'etat du joueur blanc
        this.tourSuivant();
        this.checkEtatPartie(true);	//Puis celui du joueur noir
        if(this.getTourDuJoueur().estEnEchec())	//Si le joueur noir est en echec alors que c'est normalement au joueur blanc de commencer
        {
        	System.out.println("Attention le joueur noir commence car il est en echec !");
        }
        else
        {
            this.tourSuivant();
        }
        System.out.println("Tour du joueur : " + this.tourDuJoueur + " | Situation d'echecs : " + this.tourDuJoueur.estEnEchec());
    }

    public void reintPartie() {
        this.plateau.reinitPlateau();
        this.tourDuJoueur = this.joueurBlanc;
    }

    public Joueur autreJoueur(Joueur j) {
        if (j.getCouleur().equals(Color.white)) {
            return this.joueurNoir;
        } else {
            return this.joueurBlanc;
        }
    }

    public void tourSuivant() {
        this.tourDuJoueur = this.autreJoueur(this.tourDuJoueur);
    }

    public Joueur getJoueurBlanc() {
        return joueurBlanc;
    }

    public Joueur getJoueurNoir() {
        return joueurNoir;
    }

    public Interface getAffichage() {
        return this.fenetre;
    }

    public Plateau getPlateau() {
        return plateau;
    }

    public Joueur getTourDuJoueur() {
        return tourDuJoueur;
    }

}
