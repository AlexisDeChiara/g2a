package echec;

import java.awt.*;

public class Reine extends Piece {

    public Reine(Position p, Plateau plat, String s, Color couleur) {
        super(p, plat, s, couleur);
    }

    public boolean peutSeDeplacer(Position p) {
        if (p.equals(this.getPosition()))    //Si on tente de se deplacer sur la position de depart
        {
            return false;
        } else if(this.getPlateau().piecePlateau(p) != null && this.getPlateau().piecePlateau(p).getCouleur().equals(this.getCouleur()))
    	{
    		return false;
    	} else if (Math.abs(p.getX() - this.getPosition().getX()) == Math.abs(p.getY() - this.getPosition().getY()))    //Si on se déplace bien en diagonale
        {
            return this.getPlateau().positionMilieuVide(this.getPosition(), p, Math.abs(p.getX() - this.getPosition().getX()) - 1);
        } else if (p.getX() == this.getPosition().getX())    //Deplacement sur l'axe des ordonnee
        {
            return this.getPlateau().positionMilieuVide(this.getPosition(), p, Math.abs(p.getY() - this.getPosition().getY()) - 1);
        } else if (p.getY() == this.getPosition().getY())    //Deplacement sur l'axe des abscisses
        {
            return this.getPlateau().positionMilieuVide(this.getPosition(), p, Math.abs(p.getX() - this.getPosition().getX()) - 1);
        } else {
            return false;
        }
    }
}
