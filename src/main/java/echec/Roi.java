package echec;

import java.awt.*;

public class Roi extends Piece {

    public Roi(Position p, Plateau plat, String s, Color couleur) {
        super(p, plat, s, couleur);
    }

    public boolean peutSeDeplacer(Position p) {
        if (p.equals(this.getPosition())) {
            return false;    //Si on tente de se deplacer sur la position de depart
        } else if(this.getPlateau().piecePlateau(p) != null && this.getPlateau().piecePlateau(p).getCouleur().equals(this.getCouleur()))
    	{
    		return false;
    	} else if ((Math.abs(p.getX() - this.getPosition().getX()) == 1 || Math.abs(p.getX() - this.getPosition().getX()) == 0) && (Math.abs(p.getY() - this.getPosition().getY()) == 1 || Math.abs(p.getY() - this.getPosition().getY()) == 0)) {
            Position sauvegarde = this.getPosition();    //On sauvegarde la position précedente
            Piece save = this.getPlateau().piecePlateau(p);
            boolean renvoi;
            if(save!=null)
            {
            	this.getPlateau().supprimerPiece(save);//on supprime temporairement la pièce du plateau
            }
            this.deplacement(p);                        //On deplace la position
            if (this.getPlateau().pieceMenacer(this.getPosition(), this.getCouleur()))    //Si après le déplacement la pièce est menacer
            {
            	renvoi = false;	//Si la postion est controle par une autre piece :
            } else {
            	renvoi = true;	//Si le deplacement est bien de 1 ou 0 case en abscisse et ordonnee et que la case de destination n'est pas controle
            }
            this.deplacement(sauvegarde);
            if(save != null)
            {
            	this.getPlateau().ajouterPiece(save);
            }
            return renvoi;
        } else {
            return false;
        }
    }

}
