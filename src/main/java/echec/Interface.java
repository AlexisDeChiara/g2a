package echec;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.io.File;
import java.util.HashSet;

@SuppressWarnings("serial")
public class Interface extends JFrame implements MouseListener {

    private Partie partie;
    private static JLabel echiquier;
    private JPanel plateauEchec;
    private JLabel[][] pieces;
    private JLabel messageJoueur;
    private Piece pieceDepart;
    private HashSet<Position> positionColorie;


    public Interface() {
        this.setTitle("Jeu d'échec du groupe G2A");
        this.setSize(714, 800);    //Définit sa taille
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);    //Termine le processus lorsqu'on clique sur la croix rouge
        this.setLocationRelativeTo(null);                        //PLace la fenetre au centre
        this.setLayout(null);
        this.setResizable(false);
        try {
            Image image = ImageIO.read(new File("ressources/icon.png")); //Permet de charger l'icon de la fenetre
            this.setIconImage(image);
        } catch (Exception e) {
            System.out.println("Erreur - Icon non chargé !");
        }

        this.pieceDepart = null;
        this.positionColorie = new HashSet<Position>();

        this.plateauEchec = new JPanel();    //JPanel qui contiendra le jeux d'echecs
        this.plateauEchec.setBounds(10, 10, 680, 680);
        GridLayout gl = new GridLayout(8, 8);//Layout de type grid avec 8 colonne et 8 ligne
        this.plateauEchec.setLayout(gl);
        JLabel x;
        this.pieces = new JLabel[8][8];    //On créé un tableau de JLabel de 8 par 8 permettant d'afficher les différents pions
        for (int i = 7; i >= 0; i--) {
            for (int j = 0; j <= 7; j++) {
                x = new JLabel();
                //x.setBorder(BorderFactory.createLineBorder(Color.black, 1));
                x.setHorizontalAlignment(0);
                this.pieces[i][j] = x;
                this.plateauEchec.add(x);
            }
        }
        
        this.plateauEchec.addMouseListener(this);


        Interface.echiquier = new JLabel();
        echiquier.setBounds(10, 10, 680, 680);
        Interface.echiquier.setIcon(new ImageIcon(new ImageIcon("ressources/chessboard.png").getImage().getScaledInstance(680, 680, Image.SCALE_DEFAULT)));
        this.getContentPane().add(this.plateauEchec);	//On rajoute d'abord le plateau avec les pièces
        this.getContentPane().add(Interface.echiquier);	//On rajoute ensuite l'echiquier qui vient se placer derriere
        this.plateauEchec.setOpaque(false);
        
        this.messageJoueur = new JLabel("Tour du joueur blanc");
        this.messageJoueur.setBounds(10, 689, 680, 51);
        this.messageJoueur.setAlignmentX(Component.CENTER_ALIGNMENT);
        this.getContentPane().add(this.messageJoueur);
        this.messageJoueur.setHorizontalTextPosition(SwingConstants.CENTER);
        this.messageJoueur.setHorizontalAlignment(SwingConstants.CENTER);
        this.messageJoueur.setFont(new Font("Tahoma", Font.PLAIN, 30));

        JMenuBar menuBar = new JMenuBar();
        this.setJMenuBar(menuBar);

        JMenu Partie = new JMenu("Partie");
        menuBar.add(Partie);

        JMenuItem Recommencer = new JMenuItem("Recommencer");
        Partie.add(Recommencer);
        Recommencer.addActionListener(new RecommencerListener());

        JMenuItem Charger = new JMenuItem("Charger");
        Partie.add(Charger);
        Charger.addActionListener(new ChargerListener());

    
        JMenu Personnalisation = new JMenu("Personnalisation");
        menuBar.add(Personnalisation);
        
        JMenu Couleur = new JMenu("Couleur du plateau");
        Personnalisation.add(Couleur);
        
        PersonnalisationListener perso = new PersonnalisationListener();
        
        JRadioButtonMenuItem plateauBleu = new JRadioButtonMenuItem("Bleu");
        plateauBleu.setSelected(true);
        Couleur.add(plateauBleu);
        plateauBleu.addActionListener(perso);
        
        JRadioButtonMenuItem plateauVert = new JRadioButtonMenuItem("Vert");
        Couleur.add(plateauVert);
        plateauVert.addActionListener(perso);

        JRadioButtonMenuItem plateauRouge = new JRadioButtonMenuItem("Rouge");
        Couleur.add(plateauRouge);
        plateauRouge.addActionListener(perso);
        
        JRadioButtonMenuItem plateauMarron = new JRadioButtonMenuItem("Marron");
        Couleur.add(plateauMarron);
        plateauMarron.addActionListener(perso);
        
        JRadioButtonMenuItem plateauOrange = new JRadioButtonMenuItem("Orange");
        Couleur.add(plateauOrange);
        plateauOrange.addActionListener(perso);
        
        JRadioButtonMenuItem plateauViolet = new JRadioButtonMenuItem("Violet");
        Couleur.add(plateauViolet);
        plateauViolet.addActionListener(perso);
        
        JRadioButtonMenuItem plateauRose = new JRadioButtonMenuItem("Rose");
        Couleur.add(plateauRose);
        plateauRose.addActionListener(perso);
        
        ButtonGroup group = new ButtonGroup();
        group.add(plateauBleu);
        group.add(plateauVert);
        group.add(plateauRouge);
        group.add(plateauMarron);
        group.add(plateauOrange);
        group.add(plateauViolet);
        group.add(plateauRose);
    }
    
    class ChargerListener implements ActionListener{
    	@Override
        public void actionPerformed(ActionEvent e) {
    		JFileChooser choisir= new JFileChooser();

    		int choice = choisir.showOpenDialog(choisir);

    		if (choice != JFileChooser.APPROVE_OPTION) return;
    		File fichier = choisir.getSelectedFile();
    		Partie save = partie;	//On sauvegarde la partie avant de la reintialiser au cas ou il y est une erreur
		    new Partie(save.getAffichage());
    		try
    		{
        		partie.getPlateau().chargerFichier(fichier.getAbsolutePath());	//On charge le fichier choisi
        		decolorierDeplacementPossible();
        		if(pieceDepart != null)
        		{
        			decolorierPosition(pieceDepart.getPosition());
            		pieceDepart = null;
        		}
        		effacerPlateau();
        		chargerPlateau();	//On l'affiche
        		checkEtatPartie();	//On check l'état du joueur blanc
        		partie.tourSuivant();
        		checkEtatPartie();
        		if(partie.getTourDuJoueur().estEnEchec())
        		{
        			JOptionPane.showMessageDialog(partie.getAffichage(), "Dans la sauvegarde chargé le joueur noir est en échec, c'est donc lui qui commence.","Échec", JOptionPane.INFORMATION_MESSAGE);
        		}
        		else
        		{
        			partie.tourSuivant();
        		}
        		afficherTourJoueur();
    		}catch (Exception ex)
    		{
    			partie = save;	//En cas d'erreur on recharge la sauvegarde
    			afficherMessageErreur(ex);	//Et on affiche le message d'erreur
    		}
    	}
    }
    
    class RecommencerListener implements ActionListener{

		@Override
        public void actionPerformed(ActionEvent e) {
			Object[] options = { "Oui", "Non" };
			int confirmed = JOptionPane.showOptionDialog(null, "<html><center>Voulez vous vraiment recommencer ?<html><center>", "Recommencer partie ?",
				    JOptionPane.YES_NO_OPTION, JOptionPane.WARNING_MESSAGE, null, 
				    options, options[0]);
			    if (confirmed == JOptionPane.YES_OPTION) {
			    	redemarrerPartie();
			    }
			  }
			}
    
    class PersonnalisationListener implements ActionListener{
        @Override
        public void actionPerformed(ActionEvent e) {
        	switch (e.getActionCommand()) {
    		case "Bleu":
    	        Interface.echiquier.setIcon(new ImageIcon(new ImageIcon("ressources/chessboard.png").getImage().getScaledInstance(680, 680, Image.SCALE_DEFAULT)));
    			break;
    		case "Vert":
    			Interface.echiquier.setIcon(new ImageIcon(new ImageIcon("ressources/chessboard_green.png").getImage().getScaledInstance(680, 680, Image.SCALE_DEFAULT)));
    	        break;
    		case "Rouge":
    			Interface.echiquier.setIcon(new ImageIcon(new ImageIcon("ressources/chessboard_red.png").getImage().getScaledInstance(680, 680, Image.SCALE_DEFAULT)));
    	        break;
    		case "Marron":
    			Interface.echiquier.setIcon(new ImageIcon(new ImageIcon("ressources/chessboard_brown.png").getImage().getScaledInstance(680, 680, Image.SCALE_DEFAULT)));
    	        break;
    		case "Orange":
    			Interface.echiquier.setIcon(new ImageIcon(new ImageIcon("ressources/chessboard_orange.png").getImage().getScaledInstance(680, 680, Image.SCALE_DEFAULT)));
    	        break;
    		case "Violet":
    			Interface.echiquier.setIcon(new ImageIcon(new ImageIcon("ressources/chessboard_purple.png").getImage().getScaledInstance(680, 680, Image.SCALE_DEFAULT)));
    	        break;
    		case "Rose":
    			Interface.echiquier.setIcon(new ImageIcon(new ImageIcon("ressources/chessboard_rose.png").getImage().getScaledInstance(680, 680, Image.SCALE_DEFAULT)));
    	        break;
    		default:
    			break;
    		}
        }
      }    

    @Override
    public void mouseClicked(MouseEvent e) {
        demanderDeplacement(new Position(e.getX() / 85, 7 - (e.getY() / 85)));
    }
    
    public void afficherTourJoueur()
    {
    	this.messageJoueur.setText("Tour du joueur " + this.partie.getTourDuJoueur());
    }
    
    public void redemarrerPartie()
    {
    	try
    	{
    		new Partie(this.partie.getAffichage());
	    	this.partie.getPlateau().chargerFichier("ressources/dispositionDepart.csv");	//On charge le fichier de départ
    		this.decolorierDeplacementPossible();	//On les décolories
    		if(this.pieceDepart != null)	//Si on avait séléctionné une pièce auparavant
    		{
    			this.decolorierPosition(this.pieceDepart.getPosition()); //On la décolorie
    			this.pieceDepart = null;
    		}
	    	this.effacerPlateau();	//On supprime l'affichage des pièces 
	    	this.chargerPlateau();	//On recharge les pièces
	    	this.afficherTourJoueur();	//On réaffiche le tour du joueur
    	}catch (Exception ex)
    	{
    		this.afficherMessageErreur(ex);
    	}
    }
    
    public void checkEtatPartie()
    {
    	if (this.partie.getTourDuJoueur().estEchecEtMat()) {
	        JOptionPane.showMessageDialog(this, "Le joueur "+this.partie.getTourDuJoueur() +" est en échec et mat","Échec et mat", JOptionPane.WARNING_MESSAGE);
	    	Object[] options = { "Oui", "Non", "Arrêter" };
			int choix = JOptionPane.showOptionDialog(null, "<html><center>Voulez vous recommencer ?</center><html>","Recommencer partie ?",
					JOptionPane.YES_NO_CANCEL_OPTION, JOptionPane.QUESTION_MESSAGE, null, 
				    options, options[0]);
			switch (choix) {
				case JOptionPane.YES_OPTION:
					redemarrerPartie();
					break;
				case JOptionPane.CANCEL_OPTION:
					dispose();
					break;
				case JOptionPane.NO_OPTION:
					break;
				default:
				break;
			}
	    }
    	else if (this.partie.getTourDuJoueur().estEnEchec()) {
            JOptionPane.showMessageDialog(this, "Le joueur "+this.partie.getTourDuJoueur() +" est en échec","Échec", JOptionPane.INFORMATION_MESSAGE);
    	}
	    else if (this.partie.getTourDuJoueur().estPat())
	    {
	    	JOptionPane.showMessageDialog(this, "Le joueur "+this.partie.getTourDuJoueur() +" est Pat","Pat", JOptionPane.WARNING_MESSAGE);
	    	Object[] options = { "Oui", "Non", "Fermer" };
			int choix = JOptionPane.showOptionDialog(null, "<html><center>Voulez vous abandonner ?</center><html>","Abandonner partie ?",
					JOptionPane.YES_NO_CANCEL_OPTION, JOptionPane.QUESTION_MESSAGE, null, 
				    options, options[0]);
			switch (choix) {
				case JOptionPane.YES_OPTION:
					redemarrerPartie();
				break;
				case JOptionPane.CANCEL_OPTION:
					dispose();
				default:
				break;
			}
	    }
    }

    public void demanderDeplacement(Position positionSelectionne) {
        try {
            if (this.pieceDepart == null)    //Si c'est la première position selectionné
            {
                this.pieceDepart = this.partie.getPlateau().piecePlateau(positionSelectionne);
                if (this.partie.getTourDuJoueur().possedePiece(this.pieceDepart)) {
                    this.colorierPosition(positionSelectionne, Color.red);
                    this.colorierDeplacemenPossible();
                } else {
                    this.pieceDepart = null;
                    if(this.partie.getPlateau().piecePlateau(positionSelectionne) == null)
                    {
                    	throw new Exception("Cette case est vide !");
                    }else
                    {
                    	throw new Exception("Cette piece ne vous appartient pas !");
                    }
                }
            } else {
                if (this.pieceDepart.getPosition().equals(positionSelectionne))    //Si on a selectionne la même piece
                {
                    this.decolorierPosition(positionSelectionne);
                    this.decolorierDeplacementPossible();
                    this.pieceDepart = null;
                } else {
                    Position save = this.pieceDepart.getPosition();	//On sauvegarde la position de la piece de depart

                    this.partie.getTourDuJoueur().demanderDeplacement(this.pieceDepart.getPosition(), positionSelectionne);
                    this.decolorierPosition(save);
                    this.deplacerPiece(save, positionSelectionne);
                    this.decolorierDeplacementPossible();
                    this.pieceDepart = null;
                    this.partie.tourSuivant();
                    this.afficherTourJoueur();
                    this.checkEtatPartie();
                }
            }
        } catch (Exception e) {
            this.afficherMessageErreur(e);
        }

    }

    public void colorierPosition(Position cible, Color color) {
        this.pieces[cible.getY()][cible.getX()].setOpaque(true);
        this.pieces[cible.getY()][cible.getX()].setBackground(color);
    }

    public void decolorierPosition(Position cible) {
        this.pieces[cible.getY()][cible.getX()].setOpaque(false);
        this.pieces[cible.getY()][cible.getX()].setBackground(null);
    }

    public void colorierDeplacemenPossible() {
        for (Position p : Plateau.getPositionPossible()) {
            if (this.pieceDepart.peutSeDeplacer(p) && !this.pieceDepart.getPosition().equals(p)) {
                this.colorierPosition(p, Color.yellow);
                this.positionColorie.add(p);
            }
        }
    }

    public void decolorierDeplacementPossible() {
    	if(!this.positionColorie.isEmpty())
    	{
    		for (Position p : this.positionColorie) {
                this.decolorierPosition(p);
            }
            this.positionColorie.clear();
    	}
    }

    public void afficherMessageErreur(Exception e)    //Fonction pour afficher les messages d'erreur et les exceptions
    {
        JOptionPane.showMessageDialog(this, e.getMessage(), "Erreur", JOptionPane.ERROR_MESSAGE);
    }

    public void chargerPlateau() {
        for (Piece p : this.partie.getPlateau().getPieces()) {
            this.pieces[p.getPosition().getY()][p.getPosition().getX()].setIcon(p.getImage());
		}
    }
    
    public void effacerPlateau()
    {
    	for(int i=0 ; i<8 ; i++)
    	{
    		for(int j=0 ; j<8 ; j++)
    		{
    			this.pieces[i][j].setIcon(null);
    		}
    	}
    }

    public void deplacerPiece(Position depart, Position arrive)    //Permet de deplacer une piece
    {
        this.pieces[arrive.getY()][arrive.getX()].setIcon(this.pieces[depart.getY()][depart.getX()].getIcon());
        this.pieces[depart.getY()][depart.getX()].setIcon(null);
    }

    public void retirerPiece(Position cible)    //Permet de faire disparaitre une piece
    {
        this.pieces[cible.getY()][cible.getX()].setIcon(null);
    }

    public void setPartie(Partie p) {
        this.partie = p;
    }

    @Override
    public void mousePressed(MouseEvent e) {
        // TODO Auto-generated method stub

    }

    @Override
    public void mouseReleased(MouseEvent e) {
        // TODO Auto-generated method stub

    }

    @Override
    public void mouseEntered(MouseEvent e) {
        // TODO Auto-generated method stub

    }

    @Override
    public void mouseExited(MouseEvent e) {
        // TODO Auto-generated method stub

    }

	public void setPieceDepart(Piece pieceDepart) {
		this.pieceDepart = pieceDepart;
	}
    
    
}
