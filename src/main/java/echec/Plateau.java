package echec;

import java.awt.*;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.util.HashSet;

public class Plateau {

    private static HashSet<Position> positionPossible;    //Collection de position contenant toute les positions possible sur le plateau

    private Partie partie;
    private HashSet<Piece> pieces;
    private Joueur joueurBlanc;    //Joueur contenant les pièces blanches
    private Joueur joueurNoir;    //Joueur contenant les pièces noires
    private String[][] plateauConsole;

    public Plateau(Partie game) {
        this.partie = game;
        this.pieces = new HashSet<Piece>();
        this.joueurBlanc = new Joueur(Color.white, this, this.partie);
        this.joueurNoir = new Joueur(Color.black, this, this.partie);
    }

    public static void chargerPositionPossible()    //Permet d'initialiser la collection contenant toute les positions possibles
    {
        positionPossible = new HashSet<Position>();
        for (int i = 0; i < 8; i++) {
            for (int j = 0; j < 8; j++) {
                positionPossible.add(new Position(j, i));
            }
        }
    }

    public static HashSet<Position> getPositionPossible() {
        return positionPossible;
    }

    public boolean positionMilieuVide(Position debut, Position arrive, int nombrePos)    //Verifie si les position entre debut et arrive sont vides, nombrePos represente le nombre de position a verifier
    {
        int x = debut.getX();
        int y = debut.getY();
        for (int i = 0; i < nombrePos; i++)    //On verifie pour chaque postion entre la postion de depart et celle d'arrive (non comprise)
        {
            if (arrive.getX() - debut.getX() > 0)    //Deplacement x+1
            {
                x++;
            } else if (arrive.getX() - debut.getX() < 0)//Deplacement x-1
            {
                x--;
            }
            if (arrive.getY() - debut.getY() > 0)    //Deplacement y+1
            {
                y++;
            } else if (arrive.getY() - debut.getY() < 0)//Deplacement y-1
            {
                y--;
            }
            if (this.piecePlateau(new Position(x, y)) != null) {
                return false;    //Si une piece ce trouve sur son chemin on renvoie faux
            }
        }
        return true;
    }

    public boolean pieceMenacer(Position pos, Color c)    //Cherche si la piece de couleur c a la position pos est menacer
    {
        Joueur j = null;
        if (c.equals(Color.white)) {
            j = this.joueurNoir;    //Si la piece est blanche on cherche parmi les pieces du joueur noir
        } else {
            j = this.joueurBlanc;    //Si la piece est noir on cherche parmi les pieces du joueur blanc
        }
        for (Piece p : j.getSetPiece()) {
            if (p.peutSeDeplacer(pos)) {
                return true;
            }
        }
        return false;
    }

    public void initPlateau() {
        System.out.println("Si vous voulez charger une sauvegarde tapez 'charger', sinon tapez 'démarrer' pour en démarrer une nouvelle :");
        String str = Partie.scanner.nextLine();
        while (!str.toLowerCase().equals("charger") && !str.toLowerCase().equals("démarrer") && !str.toLowerCase().equals("demarrer"))    //Tant que la valeur saisi est incorrect
        {
            System.out.println("Saisi incorrect ! Taper 'charger' pour charger une sauvegarde, sinon tapez démarrer pour en démarrer une nouvelle :");
            str = Partie.scanner.nextLine();
        }

        if (str.toLowerCase().equals("charger"))    //Si on souhaite charger une sauvegarde :
        {
            System.out.println("Saisir l'adresse de la sauvegarde :");
            str = Partie.scanner.nextLine();
            while (!new File(str.toLowerCase()).isFile())    //Tant que le fichier n'existe pas
            {
                System.out.println("Le fichier n'existe pas ! Saisir l'adresse de la sauvegarde ou taper démarrer pour en démarrer une nouvelle partie :");
                str = Partie.scanner.nextLine();
                if (str.toLowerCase().equals("démarrer") || str.toLowerCase().equals("demarrer")) {
                    str = "ressources/dispositionDepart.csv";
                }
            }

            try    //Si le fichier existe on essaie de le charger
            {
                chargerFichier(str);
                System.out.println("Chargement reussi !\n");    //Si il n'y a pas d'erreur dans le chargement cette ligne s'execute
            } catch (Exception e)        //Si le fichier n'est pas correct :
            {
                System.out.println(e.getMessage() + "\nVeuillez recommencer !");
                this.reinitPlateau();    //On efface ce qu'on a deja charger
                this.initPlateau();
            }
        } else    //Si l'on souhaite demarrer une nouvelle partie :
        {
            try {
                chargerFichier("ressources/dispositionDepart.csv");
                System.out.println("Chargement reussi !\n");    //Si il n'y a pas d'erreur dans le chargement cette ligne s'execute
            } catch (Exception e) {
                System.out.println(e.getMessage() + "\nVeuillez recommencer !");
                this.reinitPlateau();    //On efface ce qu'on a deja charger
                this.initPlateau();
            }
        }
    }

    public void chargerFichier(String s) throws Exception {
        if (!s.substring(s.length() - 4).equals(".csv"))    //On verifie si le fichier est du bon format
        {
            throw new Exception("Fichier incorrect (format invalide) !");
        }

        FileReader fichier = new FileReader(s);
        BufferedReader reader = new BufferedReader(fichier);
        int ligne = 7;    //On commence par le haut donc on commence a la ligne 8
        int nbrPionNoir = 0;    //Permet de vérifier qu'il n'y ai pas plus de 8 pion
        int nbrPionBlanc = 0;    //Permet de vérifier qu'il n'y ai pas plus de 8 pion
        int nbrRoiNoir = 0;        //Permet de vérifier qu'il y a un seul roi
        int nbrRoiBlanc = 0;    //Permet de vérifier qu'il y a un seul roi
        Piece piece = null;
        while (reader.ready()) {
            String line = reader.readLine();
            String[] tab = line.split(",");
            if (tab.length != 8) {
                fichier.close();
                reader.close();
                throw new Exception("Fichier incorrect (trop ou pas assez de colonne) !");
            } else {
                for (int i = 0; i < 8; i++)    //On parcour la ligne
                {
                    switch (tab[i]) {
                        case "Pn":    //Pion noir
                            nbrPionNoir++;
                            piece = new Pion(new Position(i, ligne), this, tab[i], Color.BLACK);
                            this.joueurNoir.ajouterPiece(piece);
                            this.pieces.add(piece);    //On ajoute la piece crée
                            break;

                        case "Pb":    //Pion blanc
                            nbrPionBlanc++;
                            piece = new Pion(new Position(i, ligne), this, tab[i], Color.WHITE);
                            this.joueurBlanc.ajouterPiece(piece);
                            this.pieces.add(piece);    //On ajoute la piece crée
                            break;

                        case "Tn":    //Tour noire

                            piece = new Tour(new Position(i, ligne), this, tab[i], Color.BLACK);
                            this.joueurNoir.ajouterPiece(piece);
                            this.pieces.add(piece);    //On ajoute la piece crée
                            break;

                        case "Tb":    //Tour blanche
                            piece = new Tour(new Position(i, ligne), this, tab[i], Color.WHITE);
                            this.joueurBlanc.ajouterPiece(piece);
                            this.pieces.add(piece);    //On ajoute la piece crée
                            break;

                        case "Cn":    //Cavalier noir
                            piece = new Cavalier(new Position(i, ligne), this, tab[i], Color.BLACK);
                            this.joueurNoir.ajouterPiece(piece);
                            this.pieces.add(piece);    //On ajoute la piece crée
                            break;

                        case "Cb":    //Cavalier blanc
                            piece = new Cavalier(new Position(i, ligne), this, tab[i], Color.WHITE);
                            this.joueurBlanc.ajouterPiece(piece);
                            this.pieces.add(piece);    //On ajoute la piece crée
                            break;

                        case "Fn":    //Fou noir
                            piece = new Fou(new Position(i, ligne), this, tab[i], Color.BLACK);
                            this.joueurNoir.ajouterPiece(piece);
                            this.pieces.add(piece);    //On ajoute la piece crée
                            break;

                        case "Fb":    //Fou blanc
                            piece = new Fou(new Position(i, ligne), this, tab[i], Color.WHITE);
                            this.joueurBlanc.ajouterPiece(piece);
                            this.pieces.add(piece);    //On ajoute la piece crée
                            break;

                        case "RoN":    //Roi noir
                            nbrRoiNoir++;
                            piece = new Roi(new Position(i, ligne), this, tab[i], Color.BLACK);
                            this.joueurNoir.ajouterPiece(piece);
                            this.joueurNoir.setRoi(piece);
                            this.pieces.add(piece);    //On ajoute la piece crée
                            break;

                        case "RoB":    //Roi blanc
                            nbrRoiBlanc++;
                            piece = new Roi(new Position(i, ligne), this, tab[i], Color.WHITE);
                            this.joueurBlanc.ajouterPiece(piece);
                            this.joueurBlanc.setRoi(piece);
                            this.pieces.add(piece);    //On ajoute la piece crée
                            break;

                        case "ReN":    //Reine noire
                            piece = new Reine(new Position(i, ligne), this, tab[i], Color.BLACK);
                            this.joueurNoir.ajouterPiece(piece);
                            this.pieces.add(piece);    //On ajoute la piece crée
                            break;

                        case "ReB":    //Reine blanche
                            piece = new Reine(new Position(i, ligne), this, tab[i], Color.WHITE);
                            this.joueurBlanc.ajouterPiece(piece);
                            this.pieces.add(piece);    //On ajoute la piece crée
                            break;

                        case "V":    //Case vide
                            break;
                        default:    //Si inconnu
                            throw new Exception("Fichier incorrect (piece inconnue) !");
                    }
                }
            }
            ligne--;
        }
        reader.close();
        fichier.close();
        if (ligne != -1)    //Si le fichier ne contient pas 8 lignes
        {
            throw new Exception("Fichier incorrect (ligne manquante) !");
        } else if (nbrPionNoir > 8 || nbrPionBlanc > 8)    //Si il y a trop de pion
        {
            throw new Exception("Fichier incorrect (trop de pion) !");
        } else if (nbrRoiNoir != 1 || nbrRoiBlanc != 1)    //Si il y a trop ou aucoun roi
        {
            throw new Exception("Fichier incorrect (trop ou aucun Roi) !");
        } else if (this.joueurBlanc.getSetPiece().size() > 16 || this.joueurNoir.getSetPiece().size() > 16)    //Si il y a trop de pieces dans l'une des collections
        {
            throw new Exception("Fichier incorrect (trop de pieces) !");
        }
    }

    public void affichageConsole() {
        String ligne;
        for (int i = 7; i >= 0; i--) {
            ligne = (i + 1) + " |";
            for (int j = 0; j < 8; j++) {
                if (this.plateauConsole[i][j].length() == 1) {
                    ligne += " " + this.plateauConsole[i][j] + " |";
                } else if (this.plateauConsole[i][j].length() == 2) {
                    ligne += this.plateauConsole[i][j] + " |";
                } else {
                    ligne += this.plateauConsole[i][j] + "|";
                }
            }
            System.out.println(ligne);
        }
        System.out.println("    A   B   C   D   E   F   G   H");
    }

    public void chargerPlateau()    //Permet de charger le plateau en début de partie pour le console
    {
        this.plateauConsole = new String[8][8];
        for (int y = 0; y < 8; y++) {
            for (int x = 0; x < 8; x++) {
                Piece p = this.piecePlateau(new Position(x, y));
                if (p != null) {
                    this.plateauConsole[y][x] = p.getNom();
                } else {
                    this.plateauConsole[y][x] = "V";
                }
            }
        }
    }

    public void deplacerPiece(Position depart, Position arrive)    //Pour l'affichage console
    {
        this.plateauConsole[arrive.getY()][arrive.getX()] = this.plateauConsole[depart.getY()][depart.getX()];
        this.plateauConsole[depart.getY()][depart.getX()] = "V";
    }

    public void reinitPlateau()    //Permet de retirer toute les pieces du plateau
    {
        this.pieces = new HashSet<Piece>();
        this.joueurBlanc.reinitPieces();
        this.joueurNoir.reinitPieces();
    }

    public void supprimerPiece(Piece p)    //Supprime la piece de la partie
    {
        this.pieces.remove(p);        //On retire la piece du plateau
        if (this.joueurBlanc.getSetPiece().contains(p))    //Si le joueur Blanc la possede
        {
            this.joueurBlanc.retirerPiece(p);            //On la retire de sa collection
        } else                                            //Sinon elle appartient a l'autre joueur
        {
            this.joueurNoir.retirerPiece(p);            //On la retire donc de sa collection
        }
    }
    
    public void ajouterPiece(Piece p)
    {
    	this.pieces.add(p);
    	if(p.getCouleur().equals(Color.white))
    	{
    		this.joueurBlanc.ajouterPiece(p);
    	}
    	else
    	{
    		this.joueurNoir.ajouterPiece(p);
    	}
    }

    public Piece piecePlateau(Position position)    //Renvoie la piece sur le plateau se trouvant a la position demandé ou null si il n'y a rien
    {
        for (Piece p : pieces)    //On parcours les piéces du plateau
        {
            if (p.getPosition().equals(position))    //Si on trouve une piece a la position demandé
            {
                return p;                            //On la renvoi
            }
        }
        return null;                                //Si il n'y a pas de piece a cette position on renvoi null
    }

    public boolean ajouterPiecePlateau(Piece p)    //Renvoi vrai si l'opération c'est bien dérouler et inversement
    {
        return this.pieces.add(p); //Permet d'ajouter p au plateau
    }

    public boolean retirerPiecePlateau(Piece p) //Renvoi vrai si l'opération c'est bien dérouler et inversement
    {
        return this.pieces.remove(p); //Permet de retirer p du plateau
    }

    public HashSet<Piece> getPieces() {
        return pieces;
    }

    public Joueur getJoueurBlanc() {
        return this.joueurBlanc;
    }

    public Joueur getJoueurNoir() {
        return this.joueurNoir;
    }
}
