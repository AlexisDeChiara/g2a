package echec;

import java.awt.*;
import java.util.HashSet;

public class Joueur {

    private Color couleur;
    private Plateau plateau;
    private Partie partie;
    private HashSet<Piece> setPieces;//Set contenant les piéces d'un joueur
    private Piece roi;

    public Joueur(Color c, Plateau p, Partie partie) {
        this.couleur = c;
        this.plateau = p;
        this.partie = partie;
        this.setPieces = new HashSet<Piece>();
        this.roi = null;
    }

    public void demanderDeplacement(Position depart, Position arrive) throws Exception {
        Piece pieceDepart = this.pionJoueur(depart);    //On récupére le pion a la position de départ du joueur (ou null si aucun)
        if (pieceDepart != null)                            //Si il existe bien un pion a cette position
        {
            if (pieceDepart.peutSeDeplacer(arrive)) {
                Piece pieceArrive = this.plateau.piecePlateau(arrive);    //On récupere la piece a la position d'arriver sue le plateau
                if (pieceArrive == null)    //Si il n'y a pas de piece a la position d'arriver
                {
                    pieceDepart.deplacement(arrive);
                    if (this.estEnEchec())    //Si le joueur est toujour en echec
                    {
                        pieceDepart.deplacement(depart);    //On remet la piece sur la position de depart
                        throw new Exception("Vous êtes en échec !");
                    }
                } else if (pieceArrive.getCouleur().equals(this.couleur))    //Si on trouve une piece a la position d'arriver qui appartient au joueur
                {
                    throw new Exception("Déplacement sur une piece vous appartenant !");
                } else    //Sinon la piece a la position d'arriver est a l'adversaire
                {
                    pieceDepart.deplacement(arrive);                //On deplace la piece
                    Piece save = pieceArrive;
                    this.plateau.supprimerPiece(pieceArrive);	//On supprime la piece d'arrivé
                    if (this.estEnEchec())    //Si le joueur est toujour en echec
                    {
                        pieceDepart.deplacement(depart);    //On remet la piece sur la position de depart
                        this.plateau.ajouterPiece(save);	//On remet la piece d'arrivé
                        throw new Exception("Vous êtes toujours en situation d'echec !");
                    }
                }
            } else {
                throw new Exception("Déplacement impossible !");
            }
        } else { 
            throw new Exception("Vous ne possedez pas de pion a cette position !");
        }
    }

    public boolean estEnEchec()    //Si une piece adverse menace le roi
    {
        return this.plateau.pieceMenacer(this.roi.getPosition(), this.roi.getCouleur());
    }

    public boolean estPat() {
    	Position p;
        Piece piecePos;
        boolean pat = false;    //Permet de savoir si un déplacement reglementaire (pas sur une piece allie ou sur une case invalide) est impossible
        for (int i = -1; i <= 1; i++) {
            for (int j = -1; j <= 1; j++) {
                p = new Position((this.roi.getPosition().getX() + j), (this.roi.getPosition().getY() + i));
                if (p.estValide()) {
                    piecePos = this.plateau.piecePlateau(p);
                    if (piecePos == null || !piecePos.getCouleur().equals(this.couleur)) {
                        pat = true;	//Il y a un déplacement légale car la position n'est pas occupé par un piece de la même couleur ou alors la position n'est pas occupé
                        if (this.roi.peutSeDeplacer(p))    //Si le roi a une possibilité de déplacement alors il n'y a pas pat
                        {
                            return false;
                        }
                    }
                }
            }
        }
        return pat;
    }

    public boolean estEchecEtMat() {
    	if(this.estEnEchec() && this.estPat())
    	{
    		Piece menace = null;
    		int i = 0;
    		for(Piece p : this.partie.autreJoueur(this).getSetPiece())
    		{
    			if(p.peutSeDeplacer(this.roi.getPosition()))
    			{
    				menace = p;
    				i++;
    			}
    		}
    		if(i > 1)	//Si il y a plus d'une menace alors le roi est en echec et mat
    		{
    			return true;
    		}
    		else	//Sinon on cherche si une autre piece du joueur peut sauver le roi
    		{
				if(this.plateau.pieceMenacer(menace.getPosition(), menace.getCouleur()))
				{
					return false;
				}
				else
				{
					return true;
				}
    			
    		}
    	}
    	else
    	{
    		return false;
    	}
    }

    public Piece pionJoueur(Position pos)    //Permet de trouver la piece appartenant au joueur a la position
    {
        for (Piece p : this.setPieces) {
            if (p.getPosition().equals(pos)) {
                return p;
            }
        }
        return null;    //Si le joueur n'a pas de piece a la position alors on renvoi null
    }

    public void retirerPiece(Piece p)    //Retire p de la collection du joueur
    {
        this.setPieces.remove(p);
    }

    public void ajouterPiece(Piece p)    //Ajoute p a la collection du joueur
    {
        this.setPieces.add(p);
    }

    public void reinitPieces() {
        this.setPieces = new HashSet<Piece>();
        this.roi = null;
    }

    public void setRoi(Piece p) {
        this.roi = p;
    }

    public HashSet<Piece> getSetPiece() {
        return this.setPieces;
    }

    public Color getCouleur() {
        return this.couleur;
    }

    public boolean possedePiece(Piece p) {
        return this.setPieces.contains(p);
    }

    @Override
    public String toString() {
        if (this.couleur.equals(Color.white)) {
            return "blanc";
        } else {
            return "noir";
        }
    }
}
