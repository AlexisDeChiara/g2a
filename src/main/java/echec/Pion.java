package echec;

import java.awt.*;

public class Pion extends Piece {

    public Pion(Position p, Plateau plat, String s, Color couleur) {
        super(p, plat, s, couleur);
    }

    public boolean peutSeDeplacer(Position p) {
    	Piece piecePosition = this.getPlateau().piecePlateau(p);
    	if(this.getPosition().equals(p))
    	{
    		return false;
    	} else if(piecePosition != null && piecePosition.getCouleur().equals(this.getCouleur()))
    	{
    		return false;
    	} else if (p.getX() == this.getPosition().getX() && piecePosition == null)    //Si déplacement sur la même colonne et si la case de destination est vide
        {
            if (this.getCouleur().equals(Color.white))    //Si le pion est blanc
            {
                if (p.getY() == this.getPosition().getY() + 1) //Si on le déplace sur la case de devant
                {
                    return true;
                }
                else if (p.getY() == this.getPosition().getY() + 2)    //Si on le déplace deux case devant
                {
					//Si la case intermedieraire est bien vide et si le pion est sur sa case depart
					return this.getPlateau().piecePlateau(new Position(p.getX(), p.getY() - 1)) == null && this.getPosition().getY() == 1;
                } 
                else 
                {
                    return false;
                }
            }
            else    //Sinon il est noir
            {
                if (p.getY() == this.getPosition().getY() - 1) //Si on le déplace sur la case de devant
                {
                    return true;
                } 
                else if (p.getY() == this.getPosition().getY() - 2)    //Si on le déplace deux case devant
                {
					//Si la case intermedieraire est bien vide et si le pion est sur sa case depart
					return this.getPlateau().piecePlateau(new Position(p.getX(), p.getY() + 1)) == null && this.getPosition().getY() == 6;
                }
                else
                {
                    return false;
                }
            }
        }
        else if (Math.abs(p.getX() - this.getPosition().getX()) == 1 && piecePosition != null)    //Si déplacement sur le coté de 1 case et qu'il y a une piece sur la case de destination
        {
            if (this.getCouleur().equals(Color.white))    //Si le pion est blanc
            {
				//Si déplacement sur la case en diagonale devant
				return p.getY() == this.getPosition().getY() + 1;
            } 
            else    //Sinon il est noir
            {
				//Si déplacement sur la case en diagonale devant
				return p.getY() == this.getPosition().getY() - 1;
            }
        } 
        else
        {
            return false;
        }

    }
}
