package echec;

import javax.swing.*;
import java.awt.*;
import java.util.HashMap;

public abstract class Piece {

    private static HashMap<String, ImageIcon> collectionImage = new HashMap<String, ImageIcon>();    //Collection contenant toute les images possibles avec une clé l'identifiant

    private Position position;
    private Plateau plateau;
    private ImageIcon image;
    private Color couleur;
    private String nom;

    public Piece(Position p, Plateau plat, String s, Color couleur) {
        this.position = p;
        this.plateau = plat;
        this.nom = s;
        this.image = Piece.getImage(this.nom);
        this.couleur = couleur;
    }

    public static void chargerImagePiece() {
        int x = 77; //Permet de modifier la dimensions si besoin
        collectionImage.put("Tn", new ImageIcon(new ImageIcon("ressources/tn.png").getImage().getScaledInstance(x, x, Image.SCALE_DEFAULT)));    //Chargement de la tour noire
        collectionImage.put("Tb", new ImageIcon(new ImageIcon("ressources/tb.png").getImage().getScaledInstance(x, x, Image.SCALE_DEFAULT)));    //Chargement de la tour blanche
        collectionImage.put("Cn", new ImageIcon(new ImageIcon("ressources/cn.png").getImage().getScaledInstance(x, x, Image.SCALE_DEFAULT)));    //Chargement du cavalier noir
        collectionImage.put("Cb", new ImageIcon(new ImageIcon("ressources/cb.png").getImage().getScaledInstance(x, x, Image.SCALE_DEFAULT)));    //Chargement du cavalier blanc
        collectionImage.put("Fn", new ImageIcon(new ImageIcon("ressources/fn.png").getImage().getScaledInstance(x, x, Image.SCALE_DEFAULT)));    //Chargement du fou noir
        collectionImage.put("Fb", new ImageIcon(new ImageIcon("ressources/fb.png").getImage().getScaledInstance(x, x, Image.SCALE_DEFAULT)));    //Chargement du fou blanc
        collectionImage.put("Pn", new ImageIcon(new ImageIcon("ressources/pn.png").getImage().getScaledInstance(x, x, Image.SCALE_DEFAULT)));    //Chargement de la piece noire
        collectionImage.put("Pb", new ImageIcon(new ImageIcon("ressources/pb.png").getImage().getScaledInstance(x, x, Image.SCALE_DEFAULT)));    //Chargement de la piece blanche
        collectionImage.put("ReN", new ImageIcon(new ImageIcon("ressources/ren.png").getImage().getScaledInstance(x, x, Image.SCALE_DEFAULT)));    //Chargement de la reine noire
        collectionImage.put("ReB", new ImageIcon(new ImageIcon("ressources/reb.png").getImage().getScaledInstance(x, x, Image.SCALE_DEFAULT)));    //Chargement de la reine blanche
        collectionImage.put("RoN", new ImageIcon(new ImageIcon("ressources/ron.png").getImage().getScaledInstance(x, x, Image.SCALE_DEFAULT)));    //Chargement du roi noire
        collectionImage.put("RoB", new ImageIcon(new ImageIcon("ressources/rob.png").getImage().getScaledInstance(x, x, Image.SCALE_DEFAULT)));    //Chargement du roi blanc

        for (ImageIcon img : collectionImage.values())    //Permet de vérifier que toute les images on été chargé
        {
            if (img.getImageLoadStatus() != MediaTracker.COMPLETE) {
                System.out.println("L'image " + img.getDescription() + " n'a pas été chargé !");
            }
        }
    }

    public static ImageIcon getImage(String s) {
        return Piece.collectionImage.get(s);
    }

    public abstract boolean peutSeDeplacer(Position p);

    public void deplacement(Position p) {
        this.position = p;
    }

    public Position getPosition() {
        return this.position;
    }

    public Color getCouleur() {
        return this.couleur;
    }

    public Plateau getPlateau() {
        return this.plateau;
    }

    public ImageIcon getImage() {
        return this.image;
    }

    public String getNom() {
        return this.nom;
    }
}
