package echec;

import static org.junit.jupiter.api.Assertions.*;

import java.awt.Color;

import org.junit.jupiter.api.Test;

class TestJoueur {

	Partie test = new Partie();
	Plateau plateauTest = test.getPlateau();
	
	@Test
	void testEstEnEchec() {
		assertDoesNotThrow(() -> plateauTest.chargerFichier("ressources/TestEchec.csv"));
		assertTrue(plateauTest.getJoueurBlanc().estEnEchec());
	}

	@Test
	void testEstEchecEtMat() {
		assertDoesNotThrow(() -> plateauTest.chargerFichier("ressources/coup_du_berger.csv"));
		assertFalse(plateauTest.getJoueurNoir().estEchecEtMat());
		plateauTest.getJoueurBlanc().pionJoueur(new Position(7, 4)).deplacement(new Position(5, 6));
		assertTrue(plateauTest.getJoueurNoir().estEchecEtMat());
	}
	
	@Test
	void testEstPat() {
		assertDoesNotThrow(() -> plateauTest.chargerFichier("ressources/TestEstPat.csv"));
		assertTrue(plateauTest.getJoueurBlanc().estPat());
	}

	@Test
	void testDemanderDeplacement() {
		assertDoesNotThrow(() -> plateauTest.chargerFichier("ressources/coup_du_berger.csv"));
		assertDoesNotThrow(() -> test.getTourDuJoueur().demanderDeplacement(new Position(0, 1), new Position(0, 3)));
		assertThrows(Exception.class, () -> test.getTourDuJoueur().demanderDeplacement(new Position(0, 1), new Position(0, 3)));	//Demande de déplacement d'une piece qui n'existe pas
		assertThrows(Exception.class, () -> test.getTourDuJoueur().demanderDeplacement(new Position(0, 3), new Position(0, 2)));	//On essaie de faire reculer un pion (deplacement impossible)
		assertDoesNotThrow(() -> test.getTourDuJoueur().demanderDeplacement(new Position("h5"), new Position("f7")));
		test.tourSuivant();
		assertThrows(Exception.class, () -> test.getTourDuJoueur().demanderDeplacement(new Position(0, 6), new Position(0, 5)));
	}
	
	@Test
	void testPionJoueur()
	{
		assertDoesNotThrow(() -> plateauTest.chargerFichier("ressources/coup_du_berger.csv"));
		assertEquals("RoB", plateauTest.getJoueurBlanc().pionJoueur(new Position(4, 0)).getNom());
	}
	
	@Test
	void testGetCouleur()
	{
		assertDoesNotThrow(() -> plateauTest.chargerFichier("ressources/coup_du_berger.csv"));
		assertEquals(Color.white, test.getJoueurBlanc().getCouleur());
		assertEquals(Color.black, test.getJoueurNoir().getCouleur());
	}
	
	@Test
	void testReinitPieces()
	{
		assertDoesNotThrow(() -> plateauTest.chargerFichier("ressources/coup_du_berger.csv"));
		test.getJoueurBlanc().reinitPieces();
		assertEquals(0 , test.getJoueurBlanc().getSetPiece().size());
	}
}
