package echec;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class TestPlateau {
	
	Partie test = new Partie();
	Plateau plat = test.getPlateau();
	
	@Test
	void testChargerPositionPossible() {
		Plateau.chargerPositionPossible();
		assertEquals(Plateau.getPositionPossible().size(), 64);
	}

	@Test
	void testReinitPlateau() {
		assertDoesNotThrow(() -> plat.chargerFichier("ressources/dispositionDepart.csv"));
		assertNotEquals(plat.getPieces().size(), 0);
		plat.reinitPlateau();
		assertEquals(plat.getPieces().size(), 0);
	}

	@Test
	void testPiecePlateau() {
		assertDoesNotThrow(() -> plat.chargerFichier("ressources/dispositionDepart.csv"));
		assertNotNull(plat.piecePlateau(new Position(0, 0)));
	}

}
