package echec;

import static org.junit.jupiter.api.Assertions.*;

import java.awt.Color;

import org.junit.jupiter.api.Test;

class TestPion {

	Partie game = new Partie();
	Plateau plat = game.getPlateau();
	
	@Test
	void testPeutSeDeplacer() {
		assertDoesNotThrow(() -> plat.chargerFichier("ressources/TestPion.csv"));
		Piece pion1_1 = plat.piecePlateau(new Position(1, 1));
		Piece pion1_6 = plat.piecePlateau(new Position(1, 6));
		Piece pion5_1 = plat.piecePlateau(new Position(5, 1));
		Piece pion5_6 = plat.piecePlateau(new Position(5, 6));
		
		assertFalse(pion1_1.peutSeDeplacer(new Position(1, 1)));
		
		assertFalse(pion1_1.peutSeDeplacer(new Position(1, 2)));
		assertFalse(pion1_1.peutSeDeplacer(new Position(1, 0)));
		assertFalse(pion1_1.peutSeDeplacer(new Position(1, 3)));
		assertTrue(pion1_1.peutSeDeplacer(new Position(2, 2)));
		assertTrue(pion1_1.peutSeDeplacer(new Position(0, 2)));
		
		assertFalse(pion1_6.peutSeDeplacer(new Position(1, 5)));
		assertFalse(pion1_6.peutSeDeplacer(new Position(1, 7)));
		assertFalse(pion1_6.peutSeDeplacer(new Position(1, 4)));
		assertTrue(pion1_6.peutSeDeplacer(new Position(2, 5)));
		assertTrue(pion1_6.peutSeDeplacer(new Position(0, 5)));

		assertTrue(pion5_1.peutSeDeplacer(new Position(5, 3)));
		assertTrue(pion5_1.peutSeDeplacer(new Position(5, 2)));
		assertFalse(pion5_1.peutSeDeplacer(new Position(4, 2)));
		assertFalse(pion5_1.peutSeDeplacer(new Position(6, 2)));
		
		assertTrue(pion5_6.peutSeDeplacer(new Position(5, 4)));
		assertTrue(pion5_6.peutSeDeplacer(new Position(5, 5)));
		assertFalse(pion5_6.peutSeDeplacer(new Position(4, 5)));
		assertFalse(pion5_6.peutSeDeplacer(new Position(6, 5)));
		


	}

	@Test
	void testPion() {
		Pion pion1 = new Pion(new Position(2, 3), plat, "Pn", Color.black);
		Pion pion2 = new Pion(new Position(2, 3), plat, "Pb", Color.white);
	}
}
