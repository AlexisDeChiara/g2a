package echec;

import static org.junit.jupiter.api.Assertions.*;

import java.awt.Color;

import org.junit.jupiter.api.Test;

public class TestCavalier {
	
	Partie game = new Partie();
	Plateau plat = game.getPlateau();
	
	@Test
	void testPeutSeDeplacer() {
		assertDoesNotThrow(() -> plat.chargerFichier("ressources/TestCavalier.csv"));
		Piece cavalier2_5 = plat.piecePlateau(new Position(2, 5));
		Piece cavalier5_2 = plat.piecePlateau(new Position(5, 2));
		
		assertFalse(cavalier2_5.peutSeDeplacer(new Position(2, 5)));
		
		assertTrue(cavalier2_5.peutSeDeplacer(new Position(1, 3)));
		assertTrue(cavalier2_5.peutSeDeplacer(new Position(1, 7)));
		assertTrue(cavalier2_5.peutSeDeplacer(new Position(0, 6)));
		assertTrue(cavalier2_5.peutSeDeplacer(new Position(0, 4)));
		assertTrue(cavalier2_5.peutSeDeplacer(new Position(3, 3)));
		assertTrue(cavalier2_5.peutSeDeplacer(new Position(4, 4)));
		assertTrue(cavalier2_5.peutSeDeplacer(new Position(3, 7)));
		assertFalse(cavalier2_5.peutSeDeplacer(new Position(4, 6)));
		assertFalse(cavalier2_5.peutSeDeplacer(new Position(7, 7)));
		
		assertFalse(cavalier5_2.peutSeDeplacer(new Position(5, 2)));
		
		assertTrue(cavalier5_2.peutSeDeplacer(new Position(4, 0)));
		assertTrue(cavalier5_2.peutSeDeplacer(new Position(6, 0)));
		assertTrue(cavalier5_2.peutSeDeplacer(new Position(7, 1)));
		assertTrue(cavalier5_2.peutSeDeplacer(new Position(7, 3)));
		assertTrue(cavalier5_2.peutSeDeplacer(new Position(6, 4)));
		assertTrue(cavalier5_2.peutSeDeplacer(new Position(4, 4)));
		assertTrue(cavalier5_2.peutSeDeplacer(new Position(3, 3)));
		assertFalse(cavalier5_2.peutSeDeplacer(new Position(3, 1)));
		assertFalse(cavalier5_2.peutSeDeplacer(new Position(7, 7)));
	}
	
	@Test
	void testCavalier() {
		Cavalier cavalier1 = new Cavalier(new Position(1, 7), plat, "Cn", Color.black);
		Cavalier cavalier2 = new Cavalier(new Position(6, 7), plat, "Cn", Color.black);
		Cavalier cavalier3 = new Cavalier(new Position(1, 0), plat, "Cb", Color.white);
		Cavalier cavalier4 = new Cavalier(new Position(6, 0), plat, "Cb", Color.white);
	}
}
