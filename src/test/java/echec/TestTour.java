package echec;

import static org.junit.jupiter.api.Assertions.*;

import java.awt.Color;

import org.junit.jupiter.api.Test;

public class TestTour {
	
	Partie game = new Partie();
	Plateau plat = game.getPlateau();
	
	@Test
	void testPeutSeDeplacer() {
		assertDoesNotThrow(() -> plat.chargerFichier("ressources/TestTour.csv"));
		Piece tour1_4 = plat.piecePlateau(new Position(1, 4));
		Piece tour4_2 = plat.piecePlateau(new Position(4, 2));
		
		assertFalse(tour1_4.peutSeDeplacer(new Position(1, 4)));
		
		assertFalse(tour1_4.peutSeDeplacer(new Position(7, 4)));
		assertFalse(tour1_4.peutSeDeplacer(new Position(0, 3)));
		assertTrue(tour1_4.peutSeDeplacer(new Position(0, 4)));
		assertTrue(tour1_4.peutSeDeplacer(new Position(1, 2)));
		assertTrue(tour1_4.peutSeDeplacer(new Position(1, 3)));
		assertTrue(tour1_4.peutSeDeplacer(new Position(1, 5)));
		assertTrue(tour1_4.peutSeDeplacer(new Position(1, 6)));
		assertTrue(tour1_4.peutSeDeplacer(new Position(1, 7)));
		assertTrue(tour1_4.peutSeDeplacer(new Position(2, 4)));
		assertTrue(tour1_4.peutSeDeplacer(new Position(3, 4)));
		assertTrue(tour1_4.peutSeDeplacer(new Position(4, 4)));
		assertTrue(tour1_4.peutSeDeplacer(new Position(5, 4)));
		
		assertFalse(tour4_2.peutSeDeplacer(new Position(4, 2)));
		
		assertFalse(tour4_2.peutSeDeplacer(new Position(5, 2)));
		assertFalse(tour4_2.peutSeDeplacer(new Position(4, 7)));
		assertTrue(tour4_2.peutSeDeplacer(new Position(0, 2)));
		assertTrue(tour4_2.peutSeDeplacer(new Position(1, 2)));
		assertTrue(tour4_2.peutSeDeplacer(new Position(2, 2)));
		assertTrue(tour4_2.peutSeDeplacer(new Position(3, 2)));
		assertTrue(tour4_2.peutSeDeplacer(new Position(4, 3)));
		assertTrue(tour4_2.peutSeDeplacer(new Position(4, 4)));
		assertTrue(tour4_2.peutSeDeplacer(new Position(4, 5)));
	}
	
	@Test
	void testTour() {
		Tour tour1 = new Tour(new Position(0, 7), plat, "Tn", Color.black);
		Tour tour2 = new Tour(new Position(7, 7), plat, "Tn", Color.black);
		Tour tour3 = new Tour(new Position(0, 0), plat, "Tb", Color.white);
		Tour tour4 = new Tour(new Position(7, 0), plat, "Tb", Color.white);
	}
}
