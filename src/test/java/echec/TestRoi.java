package echec;

import static org.junit.jupiter.api.Assertions.*;

import java.awt.Color;

import org.junit.jupiter.api.Test;

public class TestRoi {
	
	Partie game = new Partie();
	Plateau plat = game.getPlateau();
	
	@Test
	void testPeutSeDeplacer() {
		assertDoesNotThrow(() -> plat.chargerFichier("ressources/TestRoi.csv"));
		Piece roi4_6 = plat.piecePlateau(new Position(4, 6));
		Piece roi4_1 = plat.piecePlateau(new Position(4, 1));
		
		assertFalse(roi4_6.peutSeDeplacer(new Position(4, 6)));
		
		assertFalse(roi4_6.peutSeDeplacer(new Position(4, 3)));
		assertFalse(roi4_6.peutSeDeplacer(new Position(1, 2)));
		assertTrue(roi4_6.peutSeDeplacer(new Position(3, 7)));
		assertTrue(roi4_6.peutSeDeplacer(new Position(4, 7)));
		assertTrue(roi4_6.peutSeDeplacer(new Position(5, 7)));
		assertTrue(roi4_6.peutSeDeplacer(new Position(3, 6)));
		assertTrue(roi4_6.peutSeDeplacer(new Position(5, 6)));
		assertTrue(roi4_6.peutSeDeplacer(new Position(3, 5)));
		assertTrue(roi4_6.peutSeDeplacer(new Position(4, 5)));
		
		assertFalse(roi4_1.peutSeDeplacer(new Position(4, 1)));
		
		assertFalse(roi4_1.peutSeDeplacer(new Position(4, 5)));
		assertFalse(roi4_1.peutSeDeplacer(new Position(5, 7)));
		assertFalse(roi4_1.peutSeDeplacer(new Position(3, 2)));
		assertFalse(roi4_1.peutSeDeplacer(new Position(4, 2)));
		assertFalse(roi4_1.peutSeDeplacer(new Position(3, 1)));
		assertTrue(roi4_1.peutSeDeplacer(new Position(5, 1)));
		assertTrue(roi4_1.peutSeDeplacer(new Position(3, 0)));
		assertTrue(roi4_1.peutSeDeplacer(new Position(4, 0)));
	}
	
	@Test
	void testRoi() {
		Roi roi1 = new Roi(new Position(4, 7), plat, "RoN", Color.black);
		Roi roi2 = new Roi(new Position(4, 0), plat, "RoB", Color.white);
	}
}
