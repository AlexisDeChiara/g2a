package echec;

import static org.junit.jupiter.api.Assertions.*;

import java.awt.Color;

import org.junit.jupiter.api.Test;

public class TestFou {
	
	Partie game = new Partie();
	Plateau plat = game.getPlateau();
	
	@Test
	void testPeutSeDeplacer() {
		assertDoesNotThrow(() -> plat.chargerFichier("ressources/TestFou.csv"));
		Piece fou6_6 = plat.piecePlateau(new Position(6, 6));
		Piece fou3_2 = plat.piecePlateau(new Position(3, 2));
		
		assertFalse(fou6_6.peutSeDeplacer(new Position(6, 6)));
		
		assertFalse(fou6_6.peutSeDeplacer(new Position(3, 3)));
		assertFalse(fou6_6.peutSeDeplacer(new Position(1, 6)));
		assertTrue(fou6_6.peutSeDeplacer(new Position(5, 7)));
		assertTrue(fou6_6.peutSeDeplacer(new Position(7, 7)));
		assertTrue(fou6_6.peutSeDeplacer(new Position(7, 5)));
		
		assertFalse(fou3_2.peutSeDeplacer(new Position(3, 2)));
		
		assertFalse(fou3_2.peutSeDeplacer(new Position(3, 5)));
		assertFalse(fou3_2.peutSeDeplacer(new Position(5, 0)));
		assertTrue(fou3_2.peutSeDeplacer(new Position(2, 3)));
		assertTrue(fou3_2.peutSeDeplacer(new Position(4, 3)));
		assertTrue(fou3_2.peutSeDeplacer(new Position(5, 4)));
		assertTrue(fou3_2.peutSeDeplacer(new Position(6, 5)));
		assertTrue(fou3_2.peutSeDeplacer(new Position(7, 6)));
		
	}
	
	@Test
	void testFou() {
		Fou fou1 = new Fou(new Position(2, 7), plat, "Fn", Color.black);
		Fou fou2 = new Fou(new Position(5, 7), plat, "Fn", Color.black);
		Fou fou3 = new Fou(new Position(2, 0), plat, "Fb", Color.white);
		Fou fou4 = new Fou(new Position(5, 0), plat, "Fb", Color.white);
	}
}
