package echec;

import static org.junit.jupiter.api.Assertions.*;

import java.awt.Color;

import org.junit.jupiter.api.Test;

public class TestReine {
	
	Partie game = new Partie();
	Plateau plat = game.getPlateau();
	
	@Test
	void testPeutSeDeplacer() {
		assertDoesNotThrow(() -> plat.chargerFichier("ressources/TestReine.csv"));
		Piece reine5_5 = plat.piecePlateau(new Position(5, 5));
		Piece reine3_1 = plat.piecePlateau(new Position(3, 1));
		
		assertFalse(reine5_5.peutSeDeplacer(new Position(5, 5)));
		
		assertFalse(reine5_5.peutSeDeplacer(new Position(3, 0)));
		assertFalse(reine5_5.peutSeDeplacer(new Position(1, 5)));
		assertTrue(reine5_5.peutSeDeplacer(new Position(3, 5)));
		assertTrue(reine5_5.peutSeDeplacer(new Position(4, 5)));
		assertTrue(reine5_5.peutSeDeplacer(new Position(5, 6)));
		assertTrue(reine5_5.peutSeDeplacer(new Position(5, 7)));
		assertTrue(reine5_5.peutSeDeplacer(new Position(6, 6)));
		assertTrue(reine5_5.peutSeDeplacer(new Position(7, 7)));
		assertTrue(reine5_5.peutSeDeplacer(new Position(6, 5)));
		assertTrue(reine5_5.peutSeDeplacer(new Position(7, 5)));
		assertTrue(reine5_5.peutSeDeplacer(new Position(6, 4)));
		assertTrue(reine5_5.peutSeDeplacer(new Position(5, 0)));
		assertTrue(reine5_5.peutSeDeplacer(new Position(5, 1)));
		assertTrue(reine5_5.peutSeDeplacer(new Position(5, 2)));
		assertTrue(reine5_5.peutSeDeplacer(new Position(5, 3)));
		assertTrue(reine5_5.peutSeDeplacer(new Position(5, 4)));
		assertTrue(reine5_5.peutSeDeplacer(new Position(2, 2)));
		assertTrue(reine5_5.peutSeDeplacer(new Position(3, 3)));
		assertTrue(reine5_5.peutSeDeplacer(new Position(4, 4)));
		
		assertFalse(reine3_1.peutSeDeplacer(new Position(3, 1)));
		
		assertFalse(reine3_1.peutSeDeplacer(new Position(7, 2)));
		assertFalse(reine3_1.peutSeDeplacer(new Position(5, 1)));
		assertTrue(reine3_1.peutSeDeplacer(new Position(2, 0)));
		assertTrue(reine3_1.peutSeDeplacer(new Position(3, 0)));
		assertTrue(reine3_1.peutSeDeplacer(new Position(4, 0)));
		assertTrue(reine3_1.peutSeDeplacer(new Position(2, 2)));
		assertTrue(reine3_1.peutSeDeplacer(new Position(1, 3)));
		assertTrue(reine3_1.peutSeDeplacer(new Position(0, 4)));
		assertTrue(reine3_1.peutSeDeplacer(new Position(3, 2)));
		assertTrue(reine3_1.peutSeDeplacer(new Position(3, 3)));
		assertTrue(reine3_1.peutSeDeplacer(new Position(3, 4)));
		assertTrue(reine3_1.peutSeDeplacer(new Position(3, 5)));
		assertTrue(reine3_1.peutSeDeplacer(new Position(3, 6)));
		assertTrue(reine3_1.peutSeDeplacer(new Position(3, 7)));
		assertTrue(reine3_1.peutSeDeplacer(new Position(4, 2)));
		assertTrue(reine3_1.peutSeDeplacer(new Position(5, 3)));
		assertTrue(reine3_1.peutSeDeplacer(new Position(6, 4)));
		assertTrue(reine3_1.peutSeDeplacer(new Position(7, 5)));
	}
	
	@Test
	void testReine() {
		Reine reine1 = new Reine(new Position(3, 7), plat, "ReN", Color.black);
		Reine reine2 = new Reine(new Position(3, 0), plat, "ReB", Color.white);
	}
}
