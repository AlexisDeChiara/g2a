package echec;

import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.Test;

class TestPosition {
	
	Position p = new Position(1, 2);
	
	@Test
	void testPositionIntInt() {
		assertEquals(p.getX(), 1);
		assertEquals(p.getY(), 2);
	}

	@Test
	void testPositionString() throws Exception{
		assertThrows(Exception.class, () -> new Position("123"));	//Test pour une chaine non �gale � "2"
		assertThrows(Exception.class, () -> new Position("(2"));	//Test pour le premier charact�re inferieur � "a"
		assertThrows(Exception.class, () -> new Position("j2"));	//Test pour le premier charact�re superieur � "h"
		assertThrows(Exception.class, () -> new Position("a9"));	//Test pour le deuxi�me charact�re superieur � "8"
		assertThrows(Exception.class, () -> new Position("a0"));	//Test pour le deuxi�me charact�re inferieur � "0"
		assertEquals(p, new Position("b3"));
	}

	@Test
	void testGetX() {
		assertEquals(p.getX(), 1);
	}

	@Test
	void testGetY() {
		assertEquals(p.getY(), 2);
	}

	@Test
	void testEqualsObject() {
		assertTrue(p.equals(new Position(1, 2)));
		assertFalse(p.equals(new Position(4,2)));
		assertFalse(p.equals(new Position(1,3)));
	}

	@Test
	void testToString() {
		assertEquals(p.toString(), "[1, 2]");
	}
	
	@Test
	void testEstValide() {
		assertFalse(new Position(-1, 0).estValide());
		assertFalse(new Position(0, -1).estValide());
		assertFalse(new Position(0, 8).estValide());
		assertFalse(new Position(8, 0).estValide());
		assertTrue(p.estValide());
	}
}
