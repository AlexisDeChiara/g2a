package echec;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class TestPartie {

	@Test
	void testCommandeDeplacement() {
		Partie game = new Partie();
		Plateau plat = game.getPlateau();
		assertDoesNotThrow(() -> plat.chargerFichier("ressources/dispositionDepart.csv"));
		assertThrows(Exception.class, () -> game.commandeDeplacement(null));
		assertThrows(Exception.class, () -> game.commandeDeplacement("a"));
		assertThrows(Exception.class, () -> game.commandeDeplacement("aaaaa"));
		assertDoesNotThrow(() -> game.commandeDeplacement("a2 a4"));
	}

}
