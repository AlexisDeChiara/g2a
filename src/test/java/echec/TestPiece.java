package echec;

import static org.junit.jupiter.api.Assertions.*;
import java.awt.Color;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

class TestPiece {
	
	Partie game = new Partie();
	Plateau plat = game.getPlateau();
	Piece tour = new Tour(new Position(2, 3), plat, "Tn", Color.black);
	

	@BeforeAll
	static void testChargerImagePiece() {
		Piece.chargerImagePiece();
	}
	
	@Test
	void testGetImageString() {
		assertNotEquals(null, Piece.getImage("Tn"));
		assertNotEquals(null, Piece.getImage("Tb"));
		assertNotEquals(null, Piece.getImage("Pb"));
		assertNotEquals(null, Piece.getImage("Pn"));
		assertNotEquals(null, Piece.getImage("Fb"));
		assertNotEquals(null, Piece.getImage("Fn"));
		assertNotEquals(null, Piece.getImage("Cb"));
		assertNotEquals(null, Piece.getImage("Cn"));
		assertNotEquals(null, Piece.getImage("RoB"));
		assertNotEquals(null, Piece.getImage("RoN"));
		assertNotEquals(null, Piece.getImage("ReB"));
		assertNotEquals(null, Piece.getImage("ReN"));
	}
	
	@Test
	void testDeplacement() {
		tour.deplacement(new Position(3, 3));
		assertEquals(tour.getPosition(), new Position(3, 3));
	}
	
	@Test
	void testGetPosition() {
		assertEquals(tour.getPosition(), new Position(2, 3));
	}

	@Test
	void testGetCouleur() {
		assertEquals(tour.getCouleur(), Color.black);
	}

	@Test
	void testGetPlateau() {
		assertEquals(tour.getPlateau(), plat);
	}

	@Test
	void testGetImage() {
		assertEquals(tour.getImage(), Piece.getImage(tour.getNom()));
	}

	@Test
	void testGetNom() {
		assertEquals(tour.getNom(), "Tn");
	}
}
